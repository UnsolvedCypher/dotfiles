-- if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

-- AstroCore provides a central place to modify mappings, vim options, autocommands, and more!
-- Configuration documentation can be found with `:h astrocore`
-- NOTE: We highly recommend setting up the Lua Language Server (`:LspInstall lua_ls`)
--       as this provides autocomplete and documentation while editing

---@type LazySpec
return {
  "AstroNvim/astrocore",
  ---@type AstroCoreOpts
  opts = {
    -- Configure core features of AstroNvim
    features = {
      large_buf = { size = 1024 * 500, lines = 10000 }, -- set global limits for large files for disabling features like treesitter
      autopairs = true,                                 -- enable autopairs at start
      cmp = true,                                       -- enable completion at start
      diagnostics_mode = 3,                             -- diagnostic mode on start (0 = off, 1 = no signs/virtual text, 2 = no virtual text, 3 = on)
      highlighturl = true,                              -- highlight URLs at start
      notifications = true,                             -- enable notifications at start
    },
    -- Diagnostics configuration (for vim.diagnostics.config({...})) when diagnostics are on
    diagnostics = {
      virtual_text = true,
      underline = true,
    },
    -- vim options can be configured here
    options = {
      opt = {                  -- vim.opt.<key>
        relativenumber = true, -- sets vim.opt.relativenumber
        number = true,         -- sets vim.opt.number
        spell = false,         -- sets vim.opt.spell
        signcolumn = "auto",   -- sets vim.opt.signcolumn to auto
        wrap = false,          -- sets vim.opt.wrap
        -- showtabline = vim.g.started_by_firenvim and 0 or 1,
      },
      g = { -- vim.g.<key>
        -- configure global vim variables (vim.g)
        -- NOTE: `mapleader` and `maplocalleader` must be set in the AstroNvim opts or before `lazy.setup`
        -- This can be found in the `lua/lazy_setup.lua` file
      },
    },
    -- Mappings can be configured through AstroCore as well.
    -- NOTE: keycodes follow the casing in the vimdocs. For example, `<Leader>` must be capitalized
    mappings = {
      -- first key is the mode
      i = {
        ["ht"] = { "<esc>", desc = "Escape" },
        ["hh"] = { "h", desc = "Type letter h" },

      },
      n = {
        -- second key is the lefthand side of the map
        -- mappings seen under group name "Buffer"
        ["<c-p>"] = { "<cmd>Telescope find_files<cr>", desc = "Find files" },
        ["<leader>Tt"] = { "<cmd>highlight Normal guibg=NONE<cr>", desc = "Transparency" },
        ["<leader>To"] = { "<cmd>highlight Normal guibg=#0a0e14<cr>", desc = "Opacity" },
        ["<leader>bb"] = { "<cmd>tabnew<cr>", desc = "New tab" },
        ["<leader>bc"] = { "<cmd>BufferLinePickClose<cr>", desc = "Pick to close" },
        ["<leader>bj"] = { "<cmd>BufferLinePick<cr>", desc = "Pick to jump" },
        ["<leader>bt"] = { "<cmd>BufferLineSortByTabs<cr>", desc = "Sort by tabs" },
        ["<leader>w"] = { "<cmd>w<cr>", desc = "Write" },
        ["<c-n>"] = { "<cmd>Neotree toggle<cr>", desc = "Toggle tree" },
        -- L and H to navigate buffers
        L = { function() require("astrocore.buffer").nav(vim.v.count1) end, desc = "Next buffer" },
        H = { function() require("astrocore.buffer").nav(-vim.v.count1) end, desc = "Previous buffer" },
        ["<leader>B"] = { "<cmd>BlameToggle<cr>", desc = "Toggle git blame" },


        ["<leader>fml"] = { "<cmd>CellularAutomaton make_it_rain<cr>", desc = "Make it rain" },

        -- hlslens
        n = { [[<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>]], desc = "Next search result", noremap = true, silent = true },
        N = { [[<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>]], desc = "Previous search result", noremap = true, },
        ["*"] = { [[*<Cmd>lua require('hlslens').start()<CR>]], desc = "Next occurrence under cursor", noremap = true, silent = true },
        ["#"] = { [[#<Cmd>lua require('hlslens').start()<CR>]], desc = "Next occurrence under cursor", noremap = true, silent = true },

        ["<leader>fd"] = { "<cmd>DevdocsOpenCurrentFloat<cr>", desc = "Show devdocs" },
      },
      o = {
      },
      v = {
        ["<"] = { "<gv", desc = "Unindent line" },
        [">"] = { ">gv", desc = "Indent line" },
      },
      t = {
        -- ["<esc>"] = false,
      },

    },
    on_keys = {
      auto_hlsearch = {},
    },
  },
}
