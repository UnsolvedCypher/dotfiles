-- if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

-- You can also add or configure plugins by creating files in this `plugins/` folder
-- Here are some examples:

---@type LazySpec
return {
      {
        "00sapo/visual.nvim",
        event = "VeryLazy", -- this is for making sure our keymaps are applied after the others: we call the previous mapppings, but other plugins/configs usually not!
      },
      {
        "https://github.com/chentoast/marks.nvim",
        lazy = false,
        main = "marks",
        opts = {},
      },
      { "ggandor/leap.nvim",
        lazy = false,
        config = function ()
         require('leap').add_default_mappings()
        end,
      },
      { "Shatur/neovim-ayu",
        lazy = false,
        main = "ayu",
        config = true,
      },
      {
          "svban/YankAssassin.nvim",
          config = function()
              require("YankAssassin").setup {
                  auto_normal = true, -- if true, autocmds are used. Whenever y is used in normal mode, the cursor doesn't move to start
                  auto_visual = true, -- if true, autocmds are used. Whenever y is used in visual mode, the cursor doesn't move to start
              }
              -- Optional Mappings
              vim.keymap.set({ "x", "n" }, "gy", "<Plug>(YADefault)", { silent = true })
              vim.keymap.set({ "x", "n" }, "<leader>y", "<Plug>(YANoMove)", { silent = true })
          end,
      },
      {
        "Eandrju/cellular-automaton.nvim",
        lazy = false,
      },
      {
        "nfvs/vim-perforce",
        lazy = false,
      },
      {
        "kevinhwang91/nvim-hlslens",
        lazy = false,
        opts = {
          calm_down = true,
          nearest_only = true,
        },
      },
      
      { "tpope/vim-surround", lazy = false },
      { "vim-scripts/ReplaceWithRegister", lazy = false },
      -- No need thanks to Leap
      -- { "justinmk/vim-sneak", lazy = false },
      { "earthly/earthly.vim", lazy = false },
      { "towolf/vim-helm", lazy = false },
      { "NoahTheDuke/vim-just", lazy = false },
      {
      "FabijanZulj/blame.nvim",
      config = function()
        require("blame").setup()
        end
      },
      {

          -- Lazy load firenvim
          -- Explanation: https://github.com/folke/lazy.nvim/discussions/463#discussioncomment-4819297
         -- 'glacambre/firenvim',
      
         -- lazy = false,
          -- lazy = not vim.g.started_by_firenvim,
          -- build = function()
          --     vim.fn["firenvim#install"](0)
          -- end
      },
      {
        "goolord/alpha-nvim",
        opts = function(_, opts) -- override the options using lazy.nvim
          function split(s, delimiter)
            result = {};
            for match in (s..delimiter):gmatch("(.-)"..delimiter) do
                table.insert(result, match);
            end
            return result;
          end

         opts.section.header.val = split(os.getenv("NVIM_BANNER"), "\n")
        end,
      },
      {
        "zk-org/zk-nvim",
        config = function()
          require("zk").setup({
            -- See Setup section below
          })
        end
      },
      {
        "OXY2DEV/markview.nvim",
        lazy = false,      -- Recommended
        opts = {
          modes = { "n", "i", "c" },
          hybrid_modes = { "i" },
        },
        -- ft = "markdown" -- If you decide to lazy-load anyway

        dependencies = {
            -- You will not need this if you installed the
            -- parsers manually
            -- Or if the parsers are in your $RUNTIMEPATH
            "nvim-treesitter/nvim-treesitter",

            "nvim-tree/nvim-web-devicons"
        }
      },
      {
      "folke/trouble.nvim",
      opts = {}, -- for default options, refer to the configuration section for custom setup.
      cmd = "Trouble",
      keys = {
        {
          "<leader>xx",
          "<cmd>Trouble diagnostics toggle<cr>",
          desc = "Diagnostics (Trouble)",
        },
        {
          "<leader>xX",
          "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
          desc = "Buffer Diagnostics (Trouble)",
        },
        {
          "<leader>cs",
          "<cmd>Trouble symbols toggle focus=false<cr>",
          desc = "Symbols (Trouble)",
        },
        {
          "<leader>cl",
          "<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
          desc = "LSP Definitions / references / ... (Trouble)",
        },
        {
          "<leader>xL",
          "<cmd>Trouble loclist toggle<cr>",
          desc = "Location List (Trouble)",
        },
        {
          "<leader>xQ",
          "<cmd>Trouble qflist toggle<cr>",
          desc = "Quickfix List (Trouble)",
        },
      },
    },
    {
      "artemave/workspace-diagnostics.nvim", 
      config = function()
        require("workspace-diagnostics").setup({})
        vim.api.nvim_set_keymap('n', '<space>xa', '', {
          noremap = true,
          callback = function()
            for _, client in ipairs(vim.lsp.buf_get_clients()) do
              require("workspace-diagnostics").populate_workspace_diagnostics(client, 0)
            end
          end
        })
      end
    },
    {
      "luckasRanarison/nvim-devdocs",
      dependencies = {
          "nvim-lua/plenary.nvim",
          "nvim-telescope/telescope.nvim",
          "nvim-treesitter/nvim-treesitter",
        },

      opts = {}
    },
}
