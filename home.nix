{ config, pkgs, lib, ... }:
let
  env = (import ./.env.nix);
  homeDirectory = if pkgs.stdenv.isLinux then "/home/${env.username}" else "/Users/${env.username}";
  linuxOnly = x: if pkgs.stdenv.isLinux then x else null;
  macOnly = x: if pkgs.stdenv.isDarwin then x else null;
  workOnly = x: if env.work then x else null;
  filterNulls = builtins.filter (item: item != null);
  mapFishPlugins = map (plugin: { name = plugin.name; src = plugin.src; });
  modernFish = import (builtins.fetchTarball {
    url = "https://github.com/nixos/nixpkgs/archive/9267a6f407bcb7932667c1eed2aee0491c77a413.tar.gz";
  }) {};
  # customNixpkgs = import (builtins.fetchTarball {
  #   url = "https://github.com/someuser/nixpkgs/archive/some-ref.tar.gz";
  # }) {};
in {
  nixpkgs.config.allowUnfree = true;
  nix.package = pkgs.nix;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';
  home.username = env.username;
  home.homeDirectory = homeDirectory;

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";

  home.file = {
    # ".config/nvim/init.vim".source = ./vimrc;
    # ".vimrc".source = ./vimrc;
    ".ideavimrc".source = ./ideavimrc;
    ".Brewfile".source = ./Brewfile;
    ".config/nvim/init.lua".source = ./init.lua;
    ".config/nvim/lua".source = ./lua;
    ".config/fish/themes/Catppuccin Mocha.theme".source = ./catppuccin.theme;
    "wallpaper.jpg".source = ./wallpaper.jpg;
  };

  home.sessionVariables = {
    XCURSOR_PATH = "/usr/share/icons";
    EDITOR = "nvim";
  };

  fonts.fontconfig.enable = true;

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  home.packages = with pkgs; filterNulls [
    # Applications
    (linuxOnly firefox-devedition-bin) # Browser
    # qutebrowser
    (macOnly flameshot) # Screenshot tool
    (linuxOnly peek) # gif capture tool
    meld # Differ
    anki-bin # Flashcards
    (linuxOnly peek) # gif capture tool
    (linuxOnly mplayer)
    (linuxOnly mpv)
    (linuxOnly kiwix) # Offline Wikipedia/StackOverflow browser
    (linuxOnly kiwix-tools) # Offline Wikipedia/StackOverflow browser
    (linuxOnly beekeeper-studio) # Database browser
    (linuxOnly gimp) # Image editor
    (linuxOnly tdrop)


    # Languages and tooling
    openssh
    autossh
    asdf-vm
    jdk11
    # jdk17
    gradle
    go
    nodejs
    nodePackages.pnpm
    nodePackages.cdk8s-cli
    nodePackages.fixjson
    pre-commit
    (python311.withPackages (p: [
      p.pyyaml
      p.docker
      p.jsonschema
      p.deepmerge
      p.kubernetes
      p.click
    ]))
    pipenv
    protobuf
    rustup
    gleam
    erlang # for gleam
    # OCaml stuff
    # UGH this package name sucks
    ocaml-ng.ocamlPackages_5_1.ocaml
    ocaml-ng.ocamlPackages_5_1.dune_3
    # ocaml-ng.ocamlPackages_5_1.opam
    # opam


    # CLI helpers
    fd # Searching for files
    eza # ls replacement
    ruplacer # Replacement utility
    dive # Docker image analysis
    so # StackOverflow
    viu # Image viewer
    dua # Disk usage analyzer
    lazygit # Git manager
    xsel # Clipboard helper
    just # Task runner
    zk # Zettelkasten note taking helper
    figlet # ASCII art generator (used for Neovim banner)
    zk # Zettelkasten note-taking assintant
    (macOnly skhd) # Keyboard shortcuts
    (workOnly awscli2) # AWS CLI
    (workOnly 
      (google-cloud-sdk.withExtraComponents( with pkgs.google-cloud-sdk.components; [
         gke-gcloud-auth-plugin
       ]))
    )
    (workOnly saml2aws) # AWS CLI
    (workOnly hadolint) # Linting for Dockerfiles
    go-2fa # 2FA cli tool
    (macOnly scrcpy) # Tool for mirroring Android to computer
    # babelfish # Allow running bash commands in fish. Replacement for bass, but not ready yet
    (workOnly jira-cli-go)

    # Themes
    (linuxOnly sweet) # GTK theme
    (linuxOnly libcanberra-gtk3) # TODO: why?
    nerd-fonts.fira-code
    noto-fonts-emoji # Emoji support
    roboto # General purpose font
    (linuxOnly gnomeExtensions.just-perfection)
    (linuxOnly gnomeExtensions.tray-icons-reloaded)

    # Containers
    (linuxOnly kubernetes)
    (linuxOnly openshift)
    minikube
    kubernetes-helm
    lazydocker
    helmfile
    (linuxOnly docker)
    podman
    (linuxOnly podman-desktop)
    (linuxOnly (pkgs.k3s.overrideAttrs (oldAttrs: {
      postInstall = oldAttrs.postInstall or "" + ''
        rm -f $out/bin/kubectl
      '';
    })))
    # (linuxOnly (pkgs.removeBin pkgs.k3s "kubectl"))
    (linuxOnly velero)
    (linuxOnly earthly)
    docker-compose

    # Editing
    # TODO: there is MacOS support
    (linuxOnly neovide)
    tree-sitter
    # nodePackages.pyright
  ];


  home.activation = {
    kubernetesAliases = lib.hm.dag.entryAfter ["writeBoundary"] ''
      /usr/bin/curl https://raw.githubusercontent.com/ahmetb/kubectl-aliases/master/.kubectl_aliases.fish -o ~/.kubectl_aliases.fish
    '';

    homebrewInstall = lib.hm.dag.entryAfter ["writeBoundary"]
      (if pkgs.stdenv.isDarwin then "/opt/homebrew/bin/brew bundle --global --cleanup" else "");
  };


  programs.wezterm.enable = true;
  programs.wezterm.extraConfig = builtins.readFile ./wezterm.lua;
  programs.direnv.enable = true;
  programs.direnv.nix-direnv.enable = true;

  programs.bottom.enable = true;
  programs.bat.enable = true;
  programs.ripgrep.enable = true;
  programs.k9s.enable = pkgs.stdenv.isLinux;

  programs.fzf = {
    enable = true;
    # We use a custom plugin, don't want the default integration
    enableFishIntegration = false;
  };

  programs.yazi = {
    enable = true;
    # enableFishIntegration = true;
  };

  programs.neovim = {
    enable = true;
    defaultEditor = true;
  };

  programs.fish = {
    enable = true;
    package = modernFish.fish;

    shellAbbrs = {
      ls = "eza --icons";
      dsa = "docker stop (docker ps -q)";
      k = "kubectl";
      km = "kubectl config set-context --current --namespace=${env.username}";
      # Perforce
      gpl = "git p4 sync && git p4 rebase";
      gpr = "p4 reconcile && p4 revert //...";
      gpls = "p4 changes -s shelved -u $USER";
      gprms = "p4 shelve -d -c";
      gpsv = "just lint_ && just format && git p4 submit --shelve HEAD";
      gpus = "git p4 unshelve";
      gpup = "git p4 submit --shelve HEAD --update-shelve";
      gps = "just lint_ && just format && git commit -v -a --no-edit --amend && git p4 submit HEAD";
      pd = "P4DIFF='difft --color always' p4 diff | less -r";
      gpauto = "p4 resolve -am";
      dritb = "docker run --entrypoint bash -it ";
      nix-cleanup = "home-manager expire-generations -30days && nix-collect-garbage --delete-old";
      hms = "home-manager switch";
    } // import ./local-abbrs.nix;

    shellInit = ''
      # Source homebrew
      if test -e '/opt/homebrew/bin/brew'
        eval $(/opt/homebrew/bin/brew shellenv)
      end

      # Extremely important: nix will not be in PATH otherwise
      if test -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
        bass source /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh
      end

      source (cat ~/.kubectl_aliases.fish | sed -r 's/(kubectl.*) --watch/watch \1/g' | psub)

      # For whatever reason, variables not defined here don't work
      set -Ux XCURSOR_PATH /usr/share/icons
      set -Ux EDITOR nvim
      # Use the "doom" font to generate a banner for Neovim
      set -Ux NVIM_BANNER (figlet -f doom ${env.neovimBanner} | string split0)

      # Only clean up invalid history entries for later terminals
      set sponge_purge_only_on_exit true

      fish_add_path ~/.local/bin

      # Set the theme
      fish_config theme choose "ayu Mirage"
      # Turn off the greeting
      set fish_greeting

      # Custom prompt to know if on a personal or work machine
      if [ ${toString env.work} ]
        set prompt_symbol ●
      end
      # On Mac, hostname changes based on the network, so we have to use scutil
      if test (${if pkgs.stdenv.isDarwin then "scutil --get ComputerName" else "hostname"}) != ${env.hostname}
        set prompt_symbol  (set_color bryellow) ' ['(hostname) ']'
      end

      # Emulates vim's cursor shape behavior
      set fish_vi_force_cursor
      # Set the normal and visual mode cursors to a block
      set fish_cursor_default block
      # Set the insert mode cursor to a line
      set fish_cursor_insert line
      # Set the replace mode cursor to an underscore
      set fish_cursor_replace_one underscore
      # The following variable can be used to configure cursor shape in
      # visual mode, but due to fish_cursor_default, is redundant here
      set fish_cursor_visual block


      # Get vi mode
      set fish_key_bindings fish_vi_key_bindings
    '';

    interactiveShellInit =  if env.work then ''
      # Get kubectl completions
      kubectl completion fish | source
      if command -v jira > /dev/null
        jira completion fish | source
      end
    '' else "";

    # Get rid of the vi mode indicator in the prompt
    functions = {
      fish_prompt.body = import ./fish_prompt.nix;

      # We don't need to show an indicator of the vi mode, since the cursor does this already
      fish_mode_prompt.body = "";

      # ht to escape vi mode
      fish_user_key_bindings = {
        body = ''
          # Get vi mode
          set fish_key_bindings fish_vi_key_bindings

          bind -M insert -m default ht backward-char force-repaint
          bind -M insert hh 'commandline -i h'
          bind -M insert \eo 'commandline --current-token --replace -- (fd --type directory | fzf)'
          bind -M insert \cR _fzf_search_history
        '';
      };
      kgep = {
        body = ''
          set -f result (kubectl get endpoints $argv[1] -o=jsonpath='{.subsets[*].addresses[*].ip}' | tr ' ' '\n' | xargs -I % kubectl get pods --field-selector=status.podIP=% | string split0)
          echo $result
        '';
      };

      kport = {
        body = ''
          set -f result (kubectl get service | rg $argv[1] | rg NodePort | string match -r -g "(^\S+) .*:(\d+)" | string split0)
          set -f just_port (kubectl get service | rg $argv[1] | rg NodePort | head -n 1 | string match -r -g ".*:(\d+)" | xsel -b) || "Could not copy to clipboard"

          echo $result
        '';
      };

    };

    plugins = with pkgs.fishPlugins; mapFishPlugins [
      plugin-git
      fzf-fish
      sponge
      bass
    ];
  };

  programs.zellij.enable = true;

  programs.tmux = {
    enable = true;
    newSession = true;
    shortcut = "a";
    keyMode = "vi";
    shell = "${pkgs.fish}/bin/fish";
    historyLimit = 5000;
    # mouse = true;
    extraConfig = ''
      set -s copy-command 'xsel -i -b'
      set -g mouse
      set-option -sa terminal-features ",*:hyperlinks"

      # prefix + H/L to move tab left/right
      bind-key -r H swap-window -d -t -1
      bind-key -r L swap-window -d -t +1
    '';

    plugins = [
      {
        plugin = pkgs.tmuxPlugins.catppuccin;
        extraConfig = ''
          set -g @catppuccin_window_left_separator ""
          set -g @catppuccin_window_right_separator " "
          set -g @catppuccin_window_middle_separator " █"
          set -g @catppuccin_window_number_position "right"

          set -g @catppuccin_window_default_fill "number"
          set -g @catppuccin_window_default_text "#W"

          set -g @catppuccin_window_current_fill "number"
          set -g @catppuccin_window_current_text "#W"

          set -g @catppuccin_status_modules_right "user host date_time"
          # TODO: this can be removed soon, it is replaced by the above line
          # need a post-September build of the package
          set -g @catppuccin_status_modules "user host date_time"
          set -g @catppuccin_status_left_separator  " "
          set -g @catppuccin_status_right_separator ""
          set -g @catppuccin_status_right_separator_inverse "no"
          set -g @catppuccin_status_fill "icon"
          set -g @catppuccin_status_connect_separator "no"

          set -g @catppuccin_directory_text "#{pane_current_path}"
          '';
      }
    ];
  };


  dconf = {
    enable = pkgs.stdenv.isLinux;
    settings = {
      "org/gnome/shell" = {
        favorite-apps = [
          "firefox-devedition.desktop"
          "prospect-mail.desktop"
          "webex.desktop"
          "jetbrains-idea.desktop"
        ];
        enabled-extensions = [
          "just-perfection-desktop@just-perfection"
          "user-theme@gnome-shell-extensions.gcampax.github.com"
        ];
      };

      "org/gnome/settings-daemon/plugins/color" = {
        night-light-enabled = true;
      };

      "org/gnome/shell/extensions/user-theme" = {
        name = "Sweet-Dark";
      };

      "org/gnome/desktop/interface" = {
        gtk-theme = "Sweet-Dark";
        icon-theme = "breeze-dark";
        monospace-font-name = "FiraCode Nerd Font 11";

      };
      "org/gnome/desktop/background" = {
        picture-uri = "file:///${homeDirectory}/wallpaper.jpg";
      };

      "org/gnome/desktop/wm/preferences" = {
        button-layout = "appmenu:minimize,maximize,close";
      };

      "org/gnome/desktop/input-sources" = {
        sources = [
          (lib.hm.gvariant.mkTuple ["xkb" "us"])
          (lib.hm.gvariant.mkTuple ["xkb" "us+dvorak"])
          (lib.hm.gvariant.mkTuple ["ibus" "pinyin"])
        ];
      };

      "org/gnome/desktop/interface" = {
          gtk-im-module = "ibus";
      };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1" = {
        name = "Flameshot";
        command = "flameshot gui";
        binding = "Print";
      };

  };
};

  programs.terminator = {
    enable = pkgs.stdenv.isLinux;
    config = {
      global_config = {
        window_state = "fullscreen";
        tab_position = "hidden";
        hide_from_taskbar = true;
        sticky = true;
      };
      keybindings = {
        go_up = "";
        go_down = "";
        go_left = "";
        go_right = "";
        next_tab = "<Primary><Shift>Right";
        prev_tab = "<Primary><Shift>Left";
        hide_window = "<Primary>space";
        layout_launcher = "";
      };
      profiles = {
        default = {
          background_darkness = 0.88;
          background_type = "transparent";
          cursor_color = "#aaaaaa";
          font = "FiraCode Nerd Font Mono 14";
          foreground_color = "#00ffff";
          show_titlebar = false;
          use_system_font = false;
        };
      };
    };
  };

  programs.git = {
    enable = true;
    userName = env.gitName;
    userEmail = env.email;
    difftastic = {
      enable = true;
    };

    extraConfig = {
      pull.rebase = true;
      core.excludesfile = "~/.gitignore-global";
      rebase.autoStash = true;
      fetch.prune = true;
      init.defaultBranch = "main";
      push.autoSetupRemote = true;
    };
  };
}

