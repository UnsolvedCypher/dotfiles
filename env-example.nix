# This file must be named ".env.nix"
{
  username = "username"; # Your username
  gitName = "Full Name"; # Your full name, as to be displayed in git
  work = false; # Whether this is a work computer or a personal one
  email = "me@example.com"; # Your email, as to be displayed in git
  neovimBanner = "AstroNvim";
}
