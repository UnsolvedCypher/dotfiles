-- Pull in the wezterm API
local wezterm = require 'wezterm'
local mux = wezterm.mux


-- This will hold the configuration.
local config = wezterm.config_builder()

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
-- config.color_scheme = 'AdventureTime'
config.font = wezterm.font 'FiraCode Nerd Font'
config.font_size = 16
config.window_background_opacity = 0.8
config.window_decorations = "RESIZE"
wezterm.on("gui-startup", function()
  local tab, pane, window = mux.spawn_window(cmd or {})
  window:gui_window():toggle_fullscreen()
end)

config.enable_tab_bar = false;

wezterm.on('open-uri', function(window, pane, uri)
  if uri:find 'file://' then
    local file_path = uri:sub(7) -- Remove the 'file://' prefix
    pane:send_text("nvim '" .. file_path .. "'" .. "\n")
    -- prevent the default action from opening in a browser
    return false
  end
  -- otherwise, by not specifying a return value, we allow later
  -- handlers and ultimately the default action to caused the
  -- URI to be opened in the browser
end)
config.keys = {
  -- Turn off the default CMD-m Hide action, allowing CMD-m to
  -- be potentially recognized and handled by the tab
  {
    key = 's',
    mods = 'CTRL | SHIFT',
    action = wezterm.action.QuickSelect,
  },
  -- Turn off the default CMD-m Hide action, allowing CMD-m to
  -- be potentially recognized and handled by the tab
  {
    key = 'e',
    mods = 'CTRL | SHIFT',
    action = wezterm.action.QuickSelectArgs {
      label = 'open file',
      patterns = { '[a-zA-Z0-9_/-]+\\.[a-zA-Z0-9]+' },
      action = wezterm.action_callback(function(window, pane)
        local uri = window:get_selection_text_for_pane(pane)
        wezterm.open_with(uri)
      end),
    }
  },
}

return config
